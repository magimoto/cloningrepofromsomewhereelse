import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {

    public StrategyStdIn() {
    }

    @Override
    public int getInt() {
    }

    @Override
    public double getDouble() {
    }

    @Override
    public String getString() {
    }
}
